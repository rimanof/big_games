<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Flappy Bird!</title>
</head>
<body STYLE="height:100vh; padding: 0;margin: 0; display: flex; justify-content: center; align-items: center;">

<?php include($_SERVER['DOCUMENT_ROOT'].'/BIG_GAMES/php/home.php')?>

<canvas id="canvas" width="288" height="512"></canvas>

<script src="js/main.js?ver=1.0"></script>
</body>
</html>