<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css?ver=1.00001">
    <title>BigGames</title>
</head>
<body>
    <?php include 'php/games.php' ?>

    <header class="header">
        <div class="container">
            <img src="assets/LogoR.png" alt="" class="header__logo">
        </div>
    </header>
    <div class="content">
        <div class="container">
            <?php for($i=0; $i<count($arrGames); $i++): ?>
                <a href="<?php echo $arrGames[$i] -> path?>" class="content__item">
                    <img src="<?php echo $arrGames[$i] -> image?>" class="content__img">
                    <h4 class="content__title">
                        <?php echo $arrGames[$i] -> name?>
                    </h4>
                </a>
            <?php endfor;?>
        </div>
    </div>

    <script src="scripts/script.js"></script>
</body>
</html>